# moodle_assignment_downloader
### I don't know how the moodleframework works this script only mimics the normal website use and scrapes the information with help of regular expressions so there is a high chance this won't work with moodle website of your institution

## Requirements

Python libraries: requests / regex (Regular expressions) / jsons

```bash
pip install requests
pip install regex
pip install jsons
```

## Usage
Downloads moodle assignment files and stores them in a neat file structure\
based on the moodle course and order

<ul>
    <li>/home/pmos/uni
        <ul>
            <li>linear_algebra
                <ul>
                    <li>b1
                        <ul>worksheet1_vectors.pdf</ul>
                        <ul>vector_room.png</ul>
                    </li>
                    <li>b2
                        <ul>worksheet_matrix.pdf</ul>
                    </li>
                </ul>
            </li>
            <li>analysis
                <ul>
                    <li>b1
                        <ul>
                            <li>exercise_imaginary_number.pdf</li>
                        </ul>
                    </li>
                </ul>
            </li>
        </ul>
    </li>
</ul>

## Setup
Download this project and the above-mentioned dependencies\
Run the setup.py file in a terminal it should look like this:
```bash
python3 /home/pmos/Downloads/setup.py

Enter your moodle login, it should look something like this: https://moodle.????????????/login
Remove the /index.php if the links ends with it

Your moodle login link:
```
I hope this is somewhat self explanatory

# It doesn't work
As mentioned above this code is designed very specific, to work with the moodle platform I have to use.\
Nonetheless, the code should be a decent base, so you can change it to work with your moodle platform.