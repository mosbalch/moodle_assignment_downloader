import json
import os

if __name__ == "__main__":
    setup_dir = {"username": "", "password": "", "moodle_link": "", "directory": "", "course_ids": {}}

    print("Welcome to moodle file downloader 2\nThis is the setup process\n")
    print("Enter your moodle login, it should look something like this: https://moodle.????????????/login\nRemove the "
          "/index.php if the link ends with it\n")

    setup_dir["moodle_link"] = input("Your moodle login link: ")
    setup_dir["username"] = input("username: ")
    setup_dir["password"] = input("password: ")

    setup_dir["directory"] = input("\nPlease enter the directory your course folders should be in: ")

    while not os.path.exists(setup_dir["directory"]):
        print(f'{setup_dir["directory"]} is not a valid directory')
        setup_dir["directory"] = input("Please enter a valid directory: ")

    print(setup_dir["directory"])

    while True:
        courses_dir = {}

        next_entry = input("\nIf you want to add a course press enter or type done to finish the setup: ")
        if next_entry == "done":
            break

        course_name = input("\nEnter a course abbreviation: ")
        print("Enter a the id, it should be at the end of the course link you want to add and should look like this id=13312 (only type the numbers)")
        course_id = input("Enter the id of " + course_name)
        setup_dir["course_ids"][course_name] = (course_id, {})

    for names in setup_dir["course_ids"]:
        try:
            path = os.path.join(setup_dir["directory"], names)
            os.mkdir(path)
        except FileExistsError:
            print("\nOne or more folders already existed these will be used for the moodle download")

    path = os.path.join(setup_dir["directory"], ".data_files")

    try:
        os.mkdir(path)
    except FileExistsError:
        print(f"Your already have a data_files folder in {path} it will be used by the moddle assignment downloader")

    with open(os.path.join(path, "setup_data.json"), "w") as file:
        file.write(json.dumps(setup_dir, indent=4))

    os.rename(os.path.join(os.path.dirname(os.path.abspath(__file__)), "moodle_assignment_downloader.py"), os.path.join(path, "moodle_assignment_downloader.py"))

    while not os.path.exists(os.path.join(path, "moodle_assignment_downloader.py")):
        print("\nCouldn't move moodle_assignment_downloader.py to ", end="")
        print(f'{path} please do this manually')

        check = input("Type done if you moved the file: ")
