import json
import requests
import re
import os

setup_data = {}
login_payload = {}
moodle_link = ""

ansi_escape_code = {
    "black": "\x1b[30m",
    "red": "\x1b[31m",
    "green": "\x1b[32m",
    "yellow": "\x1b[33m",
    "blue": "\x1b[34m",
    "magenta": "\x1b[35m",
    "cyan": "\x1b[36m",
    "white": "\x1b[37m",
    "end": "\x1b[0m"
}

file_downloaded: bool


def color_string_maker(text: str, color: str) -> str:
    return ansi_escape_code[color] + text + ansi_escape_code["end"]


# downloads the file very important comment
def download(file_url: str, file_name: str, file_dir: str, moodle_session: requests.session()):
    print("Downloading " + color_string_maker(file_name, "cyan") + " writing to " + color_string_maker(file_dir,
                                                                                                       "cyan") + "\n")
    file_response = moodle_session.get(file_url)
    if file_response.status_code == requests.codes.ok:
        if "Content-length" in file_response.headers:
            print("Size: " + color_string_maker(
                ("{:.2f}".format(float((file_response.headers["Content-length"])) / 1000000) + "MB\n"), "green"))
        with open(file_dir + "/" + file_name, "wb") as file:
            file.write(file_response.content)
            global file_downloaded
            file_downloaded = True
        print(color_string_maker(file_name + " written to " + file_dir, "green"))
    else:
        print(color_string_maker("Couldn't download file " + file_name + " with URL: " + file_url, "red"))


def create_session_with_token() -> requests.Session:
    # Session important so the token stays the same (for the session)
    l_session = requests.session()

    # first login request to scrape login token
    login_response = l_session.get(url=setup_data["moodle_link"])

    # scrape login token from response to add it to the payload
    pattern_auth = '<input type="hidden" name="logintoken" value="\\w{32}">'
    token = re.findall(pattern_auth, login_response.text)
    token = re.findall("\\w{32}", token[0])[0]
    login_payload["logintoken"] = token
    return l_session


def get_assignment_links(response: requests.Response) -> list:
    input_string = response.text
    lines = input_string.splitlines()

    assignment_links = []
    # Finding every link with "mod" and "assign" in them to find the assignment links
    for line_number, line in enumerate(lines, start=1):
        link_list = re.findall(r'(https?://\S+")', line)
        for link in link_list:
            if "mod" in link and "assign" in link:
                if link.replace("\"", "") not in assignment_links:
                    assignment_links.append(link.replace("\"", ""))
    return assignment_links


def get_file_links(assignments_response: requests.Response) -> ([], []):
    substring = "/mod_assign/introattachment/"

    links_name = ([], [])

    lines = assignments_response.text.splitlines()
    
    # Iterate through each line
    for line_number, line in enumerate(lines, start=1):
        # Check if the substring is present in the line
        if substring in line:
            link_list = re.findall(r'(https?://\S+")', line)
            for link in link_list:
                if "mod_assign" in link and "introattachment" in link:
                    good_link = link.replace("\"", "")
                    links_name[0].append(good_link)

                    good_link = good_link[good_link.rfind("/") + 1:]
                    links_name[1].append(good_link[:good_link.find("?")])
    return links_name


def download_files():
    for course_name in setup_data["course_ids"]:
        assignment_link_list = get_assignment_links(session.get(moodle_link + setup_data["course_ids"][course_name][0]))

        assignment_index = 1
        if len(setup_data["course_ids"][course_name]) > 2:
            assignment_index = setup_data["course_ids"][course_name][2]

        for assign_link in assignment_link_list:
            path = os.path.join(setup_data["directory"], course_name + "/b" + str(assignment_index))
            if assign_link not in setup_data["course_ids"][course_name][1]:
                os.mkdir(path)
                setup_data["course_ids"][course_name][1][assign_link] = []
            assignment_index += 1
            links_names = get_file_links(session.get(assign_link))

            name_index = 0
            for file_name in links_names[1]:
                if file_name not in setup_data["course_ids"][course_name][1][assign_link]:
                    setup_data["course_ids"][course_name][1][assign_link].append(file_name)
                    download(links_names[0][name_index], file_name, path, session)
                name_index += 1


if __name__ == "__main__":
    with open(os.path.join(os.path.dirname(os.path.abspath(__file__)), "setup_data.json"), "r") as json_file:
        setup_data = json.load(json_file)

    file_downloaded = False

    login_payload = {
        'anchor': '',
        'logintoken': "",
        'username': setup_data["username"],
        'password': setup_data["password"],
        'rememberusername': 1
    }

    moodle_link: str = setup_data["moodle_link"]
    moodle_link = moodle_link.replace("login", "")
    moodle_link += "course/view.php?id="

    session = create_session_with_token()
    session.post(url=setup_data["moodle_link"] + "/index.php", data=login_payload)

    download_files()

    print("\n\n" + json.dumps(setup_data["course_ids"], indent=4))

    with open(os.path.join(os.path.dirname(os.path.abspath(__file__)), "setup_data.json"), "w") as json_file:
        json_file.write(json.dumps(setup_data, indent=4))

    if not file_downloaded:
        print(color_string_maker("Assignments are up to date none were downloaded", "magenta"))
